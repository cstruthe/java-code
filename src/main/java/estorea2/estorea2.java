package estorea2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.xml.namespace.QName;

import jdk.jfr.Description; 

import java.util.ArrayList;
import java.util.Collections;
import java.io.PrintWriter;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.io.IOException;

public class estorea2 {

    public static void main(String[] args) {
        // tar -czvf payload.tar.gz projectDir
        // declaring variables
        int i = 0;
        Scanner input = new Scanner(System.in);
        Scanner readfile = null;
        String option = "";
        String add = "";
        String searchlist;
        String[] temp = new String[6];
        String[] nothing = new String[0];
        String[] keywords = new String[1];
        String[] split = new String[2];
        String[] splitdescription;
        String[] fileinput = new String[6];
        String[] makingmap;
        int productsize = 0;
        String line;
        PrintWriter output = null;
        ArrayList<Integer> list = new ArrayList<Integer>();
        ArrayList<product> products = new ArrayList<product>();
        ArrayList<Integer> newList;
        HashMap<String, ArrayList<Integer>> map = new HashMap<String, ArrayList<Integer>>();
        helper h=new helper();

        // getting input from file
        if (args.length == 0) {
            System.out.println("Error no command line argument, exiting program");
            System.exit(0);
        }
        // tries to make file
        try {
            File myObj = new File(args[0]);
            if (myObj.createNewFile()) {
            } else {
            }
        } catch (IOException e) {
            System.out.println("An error occurred.");
            System.exit(0);
        }

        try {
            // opens a file
            readfile = new Scanner(new FileInputStream(args[0]));
        } catch (FileNotFoundException e) {
            // if file cannot be opened
            System.out.println("File not found ");
            System.out.println("Exiting Program");
            System.exit(0);
        }
        // while there are still products in the file will go through the file
        while (readfile.hasNextLine()) {
            // gets the type
            line = readfile.nextLine();
            line = line.substring(line.indexOf("'") + 1, line.length() - 1);
            // if it's a book type
            if (line.equalsIgnoreCase("book")) {
                for (i = 0; i < 6; i++) {
                    line = readfile.nextLine();
                    line = line.substring(line.indexOf("'") + 1, line.length() - 1);
                    fileinput[i] = line;

                }
                products.add(new book(Integer.parseInt(fileinput[0]), fileinput[1], fileinput[2],
                        Integer.parseInt(fileinput[3]), fileinput[4], fileinput[5]));
                // if electronic
            } else if (line.equalsIgnoreCase("electronic")) {
                for (i = 0; i < 5; i++) {
                    line = readfile.nextLine();
                    line = line.substring(line.indexOf("'") + 1, line.length() - 1);
                    fileinput[i] = line;

                }
                products.add(new electronic(Integer.parseInt(fileinput[0]), fileinput[1], fileinput[2],
                        Integer.parseInt(fileinput[3]), fileinput[4]));
            }
            // updates hashmap
            h.updatehashmap(map, fileinput[1].toLowerCase(), productsize);
            productsize++;
        }

        window w= new window(products,map,args[0]);
        w.setVisible(true);

        /*
        // while the user does not exit the program it runs this
        while (!option.equalsIgnoreCase("Quit") && !option.equalsIgnoreCase("Q")) {
            // gives user options and ask for input
            System.out.println("Main:");
            System.out.println();
            System.out.println("Please Enter a setence or a character then hit enter:");
            System.out.println("Add a product to the Book or Electronics list (Add or A) case insensitive");
            System.out.println(
                    "Search for a product from both the Book and Electronic list (Search or S) case insensitive");
            System.out.println("Exit the program (Quit or Q) case insensitive");
            option = input.nextLine();
            if (option.equalsIgnoreCase("Add") || option.equalsIgnoreCase("A")) {
                // Adding to list
                add = "";
                System.out.println("Add:");
                // while user does not enter book or electronic
                while (!add.equalsIgnoreCase("Book") && !add.equalsIgnoreCase("B")
                        && !add.equalsIgnoreCase("Electronic") && !add.equalsIgnoreCase("E")) {
                    System.out.print(
                            "Please enter whether you would like to add to the book list or electronic list (Book or B)");
                    System.out.println(", (Electronic or E) case insensitive");
                    add = input.nextLine();
                    if (!add.equalsIgnoreCase("Book") && !add.equalsIgnoreCase("B")
                            && !add.equalsIgnoreCase("Electronic") && !add.equalsIgnoreCase("E")) {
                        System.out.println("Error B, Book ,Electronic or E was not entered please try again");
                    }
                }
                // if book is what user wants to input

                if (add.equalsIgnoreCase("Book") || add.equalsIgnoreCase("B")) {
                    // taking input
                    System.out.println("please enter Product ID");
                    temp[0] = input.nextLine();
                    System.out.println("Please enter name of Book");
                    temp[1] = input.nextLine();
                    System.out.println("Please enter price of book");
                    temp[2] = input.nextLine();
                    System.out.println("Please enter year published");
                    temp[3] = input.nextLine();
                    System.out.println("Please enter author");
                    temp[4] = input.nextLine();
                    System.out.println("PLease enter publisher");
                    temp[5] = input.nextLine();
                    // checking inputs from user and that theres number in product id
                    if (!temp[0].equals("") && !temp[1].equals("") && !temp[3].equals("")
                            && temp[0].matches("[0-9]+")) {
                        // check product ID
                        i = searchforProd(products, Integer.parseInt(temp[0]));
                        // if no match is found
                        if (i == -1) {
                            // makes sure there's number in year
                            if (temp[3].matches("[0-9]+")) {
                                // makes sure year is between 1000 and 9999
                                if (Integer.parseInt(temp[3]) >= 1000 && Integer.parseInt(temp[3]) <= 9999) {
                                    // adds to list
                                    products.add(new book(Integer.parseInt(temp[0]), temp[1], temp[2],
                                            Integer.parseInt(temp[3]), temp[4], temp[5]));
                                    System.out.println("Successfully entered");
                                    // updates my hash map
                                    updatehashmap(map, temp[1].toLowerCase(), productsize);
                                    productsize++;

                                } else {
                                    // if not inbetween 1000 and 9999
                                    System.out.println("Error year is not between 1000 and 9999, returning to main");
                                }
                            } else {
                                // if year is not a number
                                System.out.println("Error year is not a number returning to main");
                            }
                        } else {
                            // if product id is not a number
                            System.out.println("error Product ID already entered returning to main");
                        }
                    } else {
                        // if there wasnt enough input
                        System.out.println(
                                "Error either product id, book name or year published were not entered correctly returning to main");

                    }
                } else if (add.equalsIgnoreCase("Electronic") || add.equalsIgnoreCase("E")) {
                    // if user wants to input electronic
                    // taking input
                    System.out.println("please enter Product ID");
                    temp[0] = input.nextLine();
                    System.out.println("Please enter name of electronic");
                    temp[1] = input.nextLine();
                    System.out.println("Please enter price of electronic");
                    temp[2] = input.nextLine();
                    System.out.println("Please enter year published");
                    temp[3] = input.nextLine();
                    System.out.println("Please enter name of maker");
                    temp[4] = input.nextLine();
                    // checking inputs from user and that theres number in product id
                    if (!temp[0].equals("") && !temp[1].equals("") && !temp[3].equals("")
                            && temp[0].matches("[0-9]+")) {
                        // check product ID
                        i = searchforProd(products, Integer.parseInt(temp[0]));
                        // if no match is found
                        if (i == -1) {
                            // makes sure there's number in year
                            if (temp[3].matches("[0-9]+")) {
                                // makes sure year is between 1000 and 9999
                                if (Integer.parseInt(temp[3]) >= 1000 && Integer.parseInt(temp[3]) <= 9999) {
                                    // adds to list
                                    products.add(new electronic(Integer.parseInt(temp[0]), temp[1], temp[2],
                                            Integer.parseInt(temp[3]), temp[4]));
                                    System.out.println("Successfully entered");
                                    updatehashmap(map, temp[1].toLowerCase(), productsize);
                                    productsize++;
                                } else {
                                    // if not inbetween 1000 and 9999
                                    System.out.println("Error year is not between 1000 and 9999, returning to main");
                                }
                            } else {
                                // if year is not a number
                                System.out.println("Error year is not a number returning to main");
                            }
                        } else {
                            // if product id is not a number
                            System.out.println("error Product ID already entered returning to main");
                        }
                    } else {
                        // if there wasnt enough input
                        System.out.println(
                                "Error either product id, book name or year published were not entered correctly returning to main");

                    }
                    System.out.println();
                }
            } else if (option.equalsIgnoreCase("Search") || option.equalsIgnoreCase("S")) {
                // taking input
                keywords = new String[1];
                System.out.println("Search:");
                System.out.println();
                System.out.println("If all inputs are left blank both the list will be show fully");
                System.out.println(
                        "Please enter Product Id you would like to search for (if blank will not be used in search)");
                temp[0] = input.nextLine();
                System.out.println(
                        "Please enter Keywords you would like to search for (if blank will not be used in search)");
                keywords[0] = input.nextLine();
                System.out.println(
                        "Please enter year time period you would like to search for (i.e 2010-2020, 2010- or -2020. if blank will not be used in search)");
                temp[2] = input.nextLine();
                // product ID
                // if input is not a number
                if (temp[0].matches("[0-9]+") || temp[0].equals("")) {
                    if (temp[0].equals("")) {
                        // if there is no input
                        i = -1;
                    } else {
                        i = Integer.parseInt(temp[0]);
                    }
                    // keyword
                    // if there is no keyword
                    if (keywords[0].equals("")) {
                        keywords = nothing;
                    } else {
                        // if there is it splits
                        split = keywords[0].split(" ");
                        keywords = split;
                    }
                    // time period
                    split = temp[2].split("-");
                    if (split.length == 1 && !split[0].equals("")) {
                        // 2020-
                        temp[3] = split[0];
                        split = new String[2];
                        split[0] = temp[3];
                        split[1] = "9999";
                    } else if (split.length == 2 && split[0].equals("")) {
                        // -2021
                        split[0] = "1000";
                    } else if (split.length == 1 && split[0].equals("")) {
                        // blank
                        split = new String[2];
                        split[0] = "1000";
                        split[1] = "9999";
                    }
                    // checks that upper and lower year are both number
                    if (split[0].matches("[0-9]+") && split[1].matches("[0-9]+")) {
                        // searches
                        search(products, i, keywords, Integer.parseInt(split[0]), Integer.parseInt(split[1]), map);
                    } else {
                        // if time period is not a number
                        System.out.println("error time period is not in numbers returning to main");
                    }
                } else {
                    // if product id is not a number
                    System.out.println("error Product Id is not a number returning to main");
                }
            } else if (option.equalsIgnoreCase("Quit") || option.equalsIgnoreCase("Q")) {
                // quiting program
                System.out.println("Thank you for using my program!");

            } else {
                // if user's input from main is not a proper
                System.out.println("Error please enter the command wanted again");
            }
        }*/
        // after the user ends the program puts back into input file
        /*try {
            // opens outputstream
            output = new PrintWriter(new FileOutputStream(args[0]));
        } catch (FileNotFoundException e) {
            // if file cannot be opened properly
            System.out.println("error opening the file");
            System.exit(0);
        }
        // will the put products into output
        for (i = 0; i < products.size(); i++) {
            // if the product is a book
            if (products.get(i) instanceof book) {
                output.println("Type = 'book'");
                // if electronic
            } else if (products.get(i) instanceof electronic) {
                output.println("Type = 'electronic'");
            }
            // then puts product in
            output.println(products.get(i));

        }
        // closes file
        output.close();*/
    }

}
