package estorea2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import jdk.jfr.Description;

import java.util.ArrayList;
import java.util.Collections;
public class book extends product {
    private String Author;
    private String Publisher;
    public book(int productID, String descriptor, String price, int year, String author, String publisher) {
        // for adding to books

        this.ProductID = productID;
        this.Descriptor = descriptor;
        this.Price = price;
        this.Year = year;
        this.Author = author;
        this.Publisher = publisher;
    }
    //for printing books
    @Override
    public String toString(){
        String padded=String.format("%06d",ProductID);
        
        return "Product ID = '"+ padded + "'\nName = '" + Descriptor + "'\nPrice = '" + Price + "'\nYear = '" +Year+"'\nAuthor = '"+ Author +"'\nPublisher = '"+ Publisher +"'";
        //return super.toString() +"'\nAuthor = '"+ Author +"'\nPublisher = '"+ Publisher +"'";
    }
    @Override
    public book clone(){
        return new book(this.ProductID,this.Descriptor,this.Price,this.Year,this.Author,this.Publisher);
        //return new book(this);
    }
}
