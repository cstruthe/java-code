package estorea2;

import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.io.PrintWriter;
import estorea2.product;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

class failure extends Exception {
    failure(String message) {
        super(message);

    }
}

 class helper {
    final static int LOWERLIMIT = 1000;
    final static int UPPERLIMIT = 9999;
    String output;
    int addelectronic(String temp[], JTextArea Display, ArrayList<product> products,
            HashMap<String, ArrayList<Integer>> map, int productsize) {
                int i = 0;
                int year=0;
        try{
            if (temp[0].equals("") || temp[1].equals("") || temp[3].equals("")||temp[0].length()!=6 ) {
                // if there wasnt enough input
                output="Error either product id, book name or year published were not entered correctly";
                throw new failure("");
            } 
            try{
                i=Integer.parseInt(temp[0]);
            }catch(NumberFormatException e){
                output="Error product id is not a number";
                throw new failure("");
            }
            // check product ID
            i = searchforProd(products, i);
            // if no match is found
            if (i != -1) {
                // if product id is already entered
                output="error Product ID already entered";
                throw new failure("");
                
            }
            // makes sure there's number in year
            try{
                year=Integer.parseInt(temp[3]);
            }catch(NumberFormatException e){
                output="Error year is not a number ";
                throw new failure("");
            }
            // makes sure year is between 1000 and 9999
                    
            if (year < LOWERLIMIT || year > UPPERLIMIT) {
                // if not inbetween 1000 and 9999
                output="Error year is not between 1000 and 9999";
               throw new failure("");
            } 
            // adds to list
            products.add(new electronic(Integer.parseInt(temp[0]), temp[1], temp[2], year,temp[4]));
            Display.setText(Display.getText()+"\n"+"Succesfully entered");
            // updates my hash map
            updatehashmap(map, temp[1].toLowerCase(), productsize);
            productsize++;
        }catch(failure e){
            Display.setText(Display.getText()+"\n"+output);
        }
       
        return productsize;
    }

    int addbook(String temp[], JTextArea Display, ArrayList<product> products,
            HashMap<String, ArrayList<Integer>> map, int productsize) {
                int i = 0;
                int year=0;
        try{
            if (temp[0].equals("") || temp[1].equals("") || temp[3].equals("")||temp[0].length()!=6 ) {
                // if there wasnt enough input
                output="Error either product id, book name or year published were not entered correctly";
                throw new failure("");
            } 
            try{
                i=Integer.parseInt(temp[0]);
            }catch(NumberFormatException e){
                output="Error product id is not a number";
                throw new failure("");
            }
            // check product ID
            i = searchforProd(products, i);
            // if no match is found
            if (i != -1) {
                // if product id is already entered
                output="error Product ID already entered";
                throw new failure("");
                
            }
            // makes sure there's number in year
            try{
                year=Integer.parseInt(temp[3]);
            }catch(NumberFormatException e){
                output="Error year is not a number ";
                throw new failure("");
            }
            // makes sure year is between 1000 and 9999
                    
            if (year < LOWERLIMIT || year > UPPERLIMIT) {
                // if not inbetween 1000 and 9999
                output="Error year is not between 1000 and 9999";
               throw new failure("");
            } 
            // adds to list
            products.add(new book(Integer.parseInt(temp[0]), temp[1], temp[2], year,temp[4],temp[5]));
            Display.setText(Display.getText()+"\n"+"Succesfully entered");
            // updates my hash map
            updatehashmap(map, temp[1].toLowerCase(), productsize);
            productsize++;

                    
                
            
        }catch(failure e){
            Display.setText(Display.getText()+"\n"+output);
        }
       
        return productsize;
    }
    void checksearch(ArrayList<product> products,HashMap<String,ArrayList<Integer>> map,String temp[],JTextArea Display){
        String split[];
        String[] nothing = new String[0];
        String keywords[];
        int upperyear;
        int loweryear;
        int i=0;

        try{
        
        if(temp[0].length()!=6&&!temp[0].equals("")){
            output="error product id is not 6 characters long";
            throw new failure("");
        }

            if (temp[0].equals("")) {
                // if there is no input
                i = -1;
            } else {
                try{
                i = Integer.parseInt(temp[0]);
                }catch(NumberFormatException e){
                    output="error product id is not a number";
                    throw new failure("");
                }
            }
        
            // keyword
            // if there is no keyword
            if (temp[1].equals("")) {
                keywords = nothing;
            } else {
                // if there is it splits
                split = temp[1].split(" ");
                keywords = split;
            }
            // time period
            
            if(temp[2].equals("")){
                temp[2]="1000";
            }
            if(temp[3].equals("")){
                temp[3]="9999";
            }
            // checks that upper and lower year are both numbe
            try{
                loweryear=Integer.parseInt(temp[2]);
                upperyear=Integer.parseInt(temp[3]);
            }catch(NumberFormatException e){
            //if time period is not a number
                output="error time period is not number";
                throw new failure("");
            }
            if(upperyear<loweryear){
                output="error end year is smaller than start year";
                throw new failure("");
            }

                // searches
                search(products, i, keywords, loweryear,upperyear, map,Display);

  
        }catch(failure e){
            Display.setText(Display.getText()+"\n"+output);
        }

    }
    static int search(ArrayList<product> estorelist, int ProductID, String[] keywords, int loweryear, int upperyear,
            HashMap<String, ArrayList<Integer>> map,JTextArea Display) {
        String[] temp;
        int i = 0;
        ArrayList<Integer> list = new ArrayList<Integer>();
        ArrayList<Integer> templist = new ArrayList<Integer>();
        ArrayList<Integer> newList = new ArrayList<Integer>();
        int[] intersection = new int[estorelist.size()];
        int matchfound = -1;
        // makes sure there is actually something in book
        if (estorelist != null) {
            if (keywords.length > 0) {
                // a for loop that sees if there are keywords in the map and at what product
                for (i = 0; i < keywords.length; i++) {
                    if (i == 0) {
                        // first time through it will set list to map
                        if (map.containsKey(keywords[i].toLowerCase()) == true) {
                            list = map.get(keywords[i].toLowerCase());
                            newList = new ArrayList<>(list);
                        } else {
                            // if the map does not contain the keyword then there is no products with that
                            // keyword
                            // and it can return to main because it will not find a match
                            return matchfound;
                        }
                    } else {
                        if (map.containsKey(keywords[i].toLowerCase()) == true) {
                            // the rest of the time through it will then put the list in a temp list
                            templist = map.get(keywords[i].toLowerCase());
                        } else {
                            // if the map does not contain the keyword then there is no products with that
                            // keyword
                            // and it can return to main because it will not find a match
                            return matchfound;
                        }
                        // will go through and compare all the lists to the first list
                        // if there's any points that match it will keep them in the list
                        newList.retainAll(templist);
                    }
                }
            }
            // if the user entered nothing
            Display.setText(Display.getText()+"\n"+"Results that match:");
            if (ProductID == -1 && keywords.length == 0 && loweryear == LOWERLIMIT && upperyear == UPPERLIMIT) {
                for (i = 0; i < estorelist.size(); i++) {

                    printproduct(estorelist.get(i),Display);
                    matchfound = i;

                }
            } else {
                // if user entered something
                for (i = 0; i < estorelist.size(); i++) {
                    // if product id is equal to the product id of book or if there is no product id
                    if (keywords.length > 0) {
                        // if there are keywords it will see if at the index if the list contain said
                        // index
                        // if it's not there it doesn't need to check the rest of the value
                        if (newList.contains(i)) {
                            if (ProductID == estorelist.get(i).ProductID || ProductID == -1) {
                                // if year is inbetween time period
                                if (estorelist.get(i).Year >= loweryear && estorelist.get(i).Year <= upperyear) {
                                    // if there are keywords
                                    printproduct(estorelist.get(i),Display);
                                    matchfound = i;
                                }
                            }
                        }
                    } else {
                        // if not keyword is given
                        if (ProductID == estorelist.get(i).ProductID || ProductID == -1) {

                            // if year is inbetween time period
                            if (estorelist.get(i).Year >= loweryear && estorelist.get(i).Year <= upperyear) {
                                // if there are keywords
                                printproduct(estorelist.get(i),Display);
                                matchfound = i;
                            }
                        }
                    }
                }
            }
        }
        return matchfound;
    }
    //TRULY OVERRIDING METHODS FOR INHERITANCE (5C)
    static void printproduct(Object estorelist,JTextArea Display) {
        // checks for type
        if (estorelist instanceof book) {
            Display.setText(Display.getText()+"\n"+"Type = 'book'");
        } else if (estorelist instanceof electronic) {
            Display.setText(Display.getText()+"\n"+"Type = 'electronic'");
        }
        // then prints out rest of list
        //EFFECTIVE USE OF POLYMORPHISM(5e)
        Display.setText(Display.getText()+"\n"+estorelist);
        Display.setText(Display.getText()+"\n");
    }

    static int searchforProd(ArrayList<product> estorelist, int ProductID) {

        int i = 0;
        // if product id is not entered
        if (ProductID != -1) {
            if (estorelist != null) {
                // goes through books and see if any value is equal to product id
                for (i = 0; i < estorelist.size(); i++) {
                    if (estorelist.get(i).ProductID == ProductID) {
                        // returns where the object was found
                        return i;
                    }

                }
            }
        }
        // if nothing was found
        return -1;
    }

    static int updatehashmap(HashMap<String, ArrayList<Integer>> map, String descriptor, int productsize) {
        ArrayList<Integer> list = new ArrayList<Integer>();
        ArrayList<Integer> newList;
        int i = 0;
        // splits the descriptor
        String splitdescription[] = descriptor.split(" ");
        // makes a new map for each keyword or update if already in the map
        for (i = 0; i < splitdescription.length; i++) {
            // if it's in the map it will get what's already in the map and then add to it
            if (map.containsKey(splitdescription[i]) == true) {
                list = map.get(splitdescription[i]);
                list.add(productsize);
                newList = new ArrayList<>(list);
                map.put(splitdescription[i], newList);
            } else {
                // if the keyword is not in the map yet
                list.clear();
                list.add(productsize);
                newList = new ArrayList<>(list);
                map.put(splitdescription[i], newList);
            }
        }
        // if properly added to the map will return 1
        return 1;

    }
    void outputtofile(ArrayList<product> products,String filename){
        PrintWriter output = null;
        int i=0;
        try {
            // opens outputstream
            output = new PrintWriter(new FileOutputStream(filename));
        } catch (FileNotFoundException e) {
            // if file cannot be opened properly
            System.exit(0);
        }
        // will the put products into output
        for (i = 0; i < products.size(); i++) {
            // if the product is a book
            if (products.get(i) instanceof book) {
                output.println("Type = 'book'");
                // if electronic
            } else if (products.get(i) instanceof electronic) {
                output.println("Type = 'electronic'");
            }
            // then puts product in
            //EFFECTIVE USE OF POLYMORPHISM(5e)
            output.println(products.get(i));

        }
        // closes file
        output.close();
    }
    
}
