package estorea2;

import java.util.HashMap;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import java.awt.CardLayout;
import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import java.awt.event.WindowListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowAdapter;
import java.awt.GridLayout;
import javax.swing.JPanel;
import java.awt.FlowLayout;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import estorea2.electronic;
import estorea2.product;

import java.awt.event.ActionListener;
import java.time.Year;
import java.awt.event.ActionEvent;

public class window extends JFrame implements ActionListener {
    // Jpanels
    private JPanel Bothbottoms;
    private JPanel Center;
    private JPanel East;
    private JPanel West;
    // Jmenuitem
    private JMenuItem Addproduct;
    private JMenuItem Searchproduct;
    private JButton Search;
    private JButton Add;
    private JButton SReset;
    private JButton Reset;
    // Jtextarea
    private JTextArea addDisplay;
    private JTextArea searchDisplay;
    // JTEXTFIELDS
    private JTextField Productid;
    private JTextField Description;
    private JTextField Price;
    private JTextField Year;
    private JTextField Author;
    private JTextField Publisher;
    private JTextField Maker;
    private JTextField eProductid;
    private JTextField eDescription;
    private JTextField eYear;
    private JTextField ePrice;
    private JTextField sProductid;
    private JTextField sDescription;
    private JTextField startyear;
    private JTextField endyear;
    // MISC
    private ArrayList<product> products;
    private HashMap<String, ArrayList<Integer>> map;
    private int productsize;
    private int bookorint;
    private String filename;

    window(ArrayList<product> tempproducts, HashMap<String, ArrayList<Integer>> tempmap, String file) {
        super("eStoreSearch");
        // initial setup
        int i = 0;
        filename = file;
        //products = new ArrayList<product>(tempproducts);
        map = new HashMap<String, ArrayList<Integer>>(tempmap);  
        products=new ArrayList<product>();
        //PRIVACY LEAK PROTECTION (5B)
        for(i=0; i<tempproducts.size(); i++){
            products.add(tempproducts.get(i).clone());
        }
        productsize = products.size();
        setSize(1000, 1000);
        // setting up inital GUI
        setTitle("eStoreSearch");
        setLayout(new BorderLayout());
        JPanel buttonPanel = new JPanel();
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        addWindowListener(new CheckOnExit());
        // commands on top part
        //
        //
        //
        //
        JMenu Commands = new JMenu("Commands");
        Addproduct = new JMenuItem("Add");
        Searchproduct = new JMenuItem("Search");
        JMenuItem Exit = new JMenuItem("Exit");
        Addproduct.addActionListener(this);
        Searchproduct.addActionListener(this);
        Exit.addActionListener(this);
        Commands.add(Addproduct);
        Commands.add(Searchproduct);
        Commands.add(Exit);
        JMenuBar bar = new JMenuBar();
        bar.add(Commands);
        setJMenuBar(bar);
        // setting up scroll bars at bottom
        //
        //
        //
        //
        addDisplay = new JTextArea(15, 30);
        addDisplay.setText("");
        addDisplay.setEditable(false);
        JScrollPane scrolltext = new JScrollPane(addDisplay);
        scrolltext.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        scrolltext.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        JPanel bottom = new JPanel();
        JLabel messageLabel = new JLabel("Messages:");
        bottom.setLayout(new BoxLayout(bottom, BoxLayout.X_AXIS));
        scrolltext.setColumnHeaderView(messageLabel);
        bottom.add(scrolltext);
        bottom.setVisible(true);
        // second scroll bar
        //
        //
        //
        searchDisplay = new JTextArea(15, 30);
        searchDisplay.setText("");
        searchDisplay.setEditable(false);
        JScrollPane scrolltext2 = new JScrollPane(searchDisplay);
        scrolltext2.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        scrolltext2.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        JPanel bottom2 = new JPanel();
        JLabel searchlabel = new JLabel("Search Results:");
        bottom2.setLayout(new BoxLayout(bottom2, BoxLayout.X_AXIS));
        scrolltext2.setColumnHeaderView(searchlabel);
        bottom2.add(scrolltext2);
        // adding them together
        Bothbottoms = new JPanel(new CardLayout());
        Bothbottoms.add(bottom, "Adding");
        Bothbottoms.add(bottom2, "Searching");
        add(Bothbottoms, BorderLayout.SOUTH);
        Bothbottoms.setVisible(false);
        // setting up center
        //
        //
        //
        //
        //
        //
        //
        //

        String text = "Welcome to eStoreSearch, Choose a command from the 'Commands' menu above for adding a product,searching a product, or quitting the program";

        JLabel test = new JLabel("<html><div style='text-align: center;'>" + text + "</div></html>");
        JPanel openingpanel = new JPanel();
        openingpanel.add(test);
        // adding panel

        JPanel adding = new JPanel();
        // adding.setLayout(new FlowLayout());

        adding.setLayout(new GridLayout(8, 1));
        JLabel nothing = new JLabel("");
        JLabel type = new JLabel("Type:");

        JLabel addinglabel = new JLabel("Adding a Product");

        String[] productsbox = { "Book", "Electronic" };
        JComboBox<String> productlist = new JComboBox<>(productsbox);

        productlist.addActionListener(this);
        Productid = new JTextField("");
        JLabel prodid = new JLabel("Product ID:");
        Description = new JTextField("");
        JLabel descriplabel = new JLabel("Description:");
        Price = new JTextField("");
        JLabel pricelabel = new JLabel("Price:");
        Year = new JTextField("");
        JLabel yearlabel = new JLabel("Year:");
        Author = new JTextField("");
        JLabel authorlabel = new JLabel("Author:");
        Publisher = new JTextField("");
        JLabel publishlabel = new JLabel("Publisher:");
        JPanel addingjlabel=new JPanel();
        addingjlabel.setLayout(new GridLayout(8,1));
        addingjlabel.add(addinglabel);
        addingjlabel.add(type);
        addingjlabel.add(prodid);
        addingjlabel.add(descriplabel);
        addingjlabel.add(pricelabel);
        addingjlabel.add(yearlabel);
        addingjlabel.add(authorlabel);
        addingjlabel.add(publishlabel);
        //adding.add(addinglabel);
        adding.add(nothing);
        //adding.add(type);
        adding.add(productlist);
       // adding.add(prodid);
        adding.add(Productid);
        //adding.add(descriplabel);
        adding.add(Description);
       // adding.add(pricelabel);
        adding.add(Price);
      //  adding.add(yearlabel);
        adding.add(Year);
       // adding.add(authorlabel);
        adding.add(Author);
      //  adding.add(publishlabel);
        adding.add(Publisher);

        // adding electronic
        JLabel enothing = new JLabel("");
        JLabel etype = new JLabel("Type:");
        JLabel eaddinglabel = new JLabel("Adding a Product:");
        String[] eproducts = { "Electronic", "Book" };
        JComboBox<String> eproductlist = new JComboBox<>(eproducts);
        eproductlist.addActionListener(this);
        eProductid = new JTextField("");
        JLabel eprodid = new JLabel("Product ID:");
        eDescription = new JTextField("");
        JLabel edescriplabel = new JLabel("Description:");
        ePrice = new JTextField("");
        JLabel epricelabel = new JLabel("Price:");
        eYear = new JTextField("");
        JLabel eyearlabel = new JLabel("Year:");
        JLabel makerlabel = new JLabel("Maker:");
        Maker = new JTextField("");
        JPanel electronic = new JPanel();
        electronic.setLayout(new GridLayout(7, 1));
        JPanel elecctroniclabels=new JPanel();
        elecctroniclabels.setLayout(new GridLayout(7,1));
        elecctroniclabels.add(eaddinglabel);
        elecctroniclabels.add(etype);
        elecctroniclabels.add(eprodid);
        elecctroniclabels.add(edescriplabel);
        elecctroniclabels.add(epricelabel);
        elecctroniclabels.add(eyearlabel);
        elecctroniclabels.add(makerlabel);
        //electronic.add(eaddinglabel);
        electronic.add(enothing);
        //electronic.add(etype);
        electronic.add(eproductlist);
        //electronic.add(eprodid);
        electronic.add(eProductid);
        //electronic.add(edescriplabel);
        electronic.add(eDescription);
        //electronic.add(epricelabel);
        electronic.add(ePrice);
        //electronic.add(eyearlabel);
        electronic.add(eYear);
        //electronic.add(makerlabel);
        electronic.add(Maker);
        // searching for product
        JPanel searching = new JPanel();
        GridLayout layout = new GridLayout(5, 1);
        layout.setHgap(10);
        searching.setLayout(layout);
        JLabel sproductlabel = new JLabel("Searching Products");
        JLabel snothing = new JLabel("");
        JLabel sproductidlabel = new JLabel("Product ID:");
        sProductid = new JTextField("");

        JLabel sdescriptionlabel = new JLabel("Description Keywords:");
        sDescription = new JTextField("");
        JLabel startyearlabel = new JLabel("Start Year:");
        startyear = new JTextField("");
        JLabel endyearlabek = new JLabel("End Year:");
        JPanel searchlabels=new JPanel();
        searchlabels.setLayout(new GridLayout(5,1));
        endyear = new JTextField("");
        searchlabels.add(sproductidlabel);
        searchlabels.add(sproductlabel);
        searchlabels.add(sdescriptionlabel);
        searchlabels.add(startyearlabel);
        searchlabels.add(endyearlabek);
        //searching.add(sproductlabel);
        searching.add(snothing);
       // searching.add(sproductidlabel);
        searching.add(sProductid);
        //searching.add(sdescriptionlabel);
        searching.add(sDescription);
        //searching.add(startyearlabel);
        searching.add(startyear);
        //searching.add(endyearlabek);
        searching.add(endyear);
        // putting it all together
        West=new JPanel(new CardLayout());
        West.add(addingjlabel,"Adding book");
        West.add(elecctroniclabels,"Adding electronic");
        West.add(searchlabels,"Searching");
        add(West,BorderLayout.WEST);
        West.setVisible(false);
        
        Center = new JPanel(new CardLayout());
        Center.add(openingpanel, "Opening");
        Center.add(adding, "Adding book");
        Center.add(electronic, "Adding electronic");
        Center.add(searching, "Searching");
        add(Center, BorderLayout.CENTER);
        // east panel
        // add
        Add = new JButton("Add");
        Reset = new JButton("Reset");
        Add.addActionListener(this);
        Reset.addActionListener(this);
        JPanel add = new JPanel();
        add.setLayout(new BoxLayout(add, BoxLayout.Y_AXIS));
        add.add(Box.createVerticalStrut(100));
        add.add(Reset);
        add.add(Box.createVerticalStrut(100));
        add.add(Add);
        // searching
        Search = new JButton("Search");
        SReset = new JButton("Reset");
        Search.addActionListener(this);
        SReset.addActionListener(this);
        JPanel searchpanel = new JPanel();
        searchpanel.setLayout(new BoxLayout(searchpanel, BoxLayout.Y_AXIS));
        searchpanel.add(Box.createVerticalStrut(100));
        searchpanel.add(SReset);
        searchpanel.add(Box.createVerticalStrut(100));
        searchpanel.add(Search);
        // putting it all togehter
        East = new JPanel(new CardLayout());
        East.add(add, "Adding");
        East.add(searchpanel, "Searching");
        add(East, BorderLayout.EAST);
        East.setVisible(false);

    }

    private class CheckOnExit extends WindowAdapter {
        public void windowClosing(WindowEvent e) {
            helper h = new helper();
            h.outputtofile(products, filename);
            System.exit(0);
        }
    }

    public void actionPerformed(ActionEvent ez) {

        Object command = ez.getSource();
        String commandstring = ez.getActionCommand();
        String temp[] = new String[6];
        CardLayout c1 = (CardLayout) (Bothbottoms.getLayout());
        CardLayout c2 = (CardLayout) (Center.getLayout());
        CardLayout c3 = (CardLayout) (East.getLayout());
        CardLayout c4=(CardLayout)(West.getLayout());
        helper h = new helper();
        if (commandstring.equals("Exit")) {
            h.outputtofile(products, filename);
            System.exit(0);
        } else if (command.equals(Addproduct)) {
            Bothbottoms.setVisible(true);
            East.setVisible(true);
            West.setVisible(true);
            c1.show(Bothbottoms, "Adding");
            c2.show(Center, "Adding book");
            c3.show(East, "Adding");
            c4.show(West,"Adding book");
            bookorint = 0;
        } else if (command.equals(Searchproduct)) {
            Bothbottoms.setVisible(true);
            East.setVisible(true);
            West.setVisible(true);
            c1.show(Bothbottoms, "Searching");
            c2.show(Center, "Searching");
            c3.show(East, "Searching");
            c4.show(West,"Searching");

        } else if (commandstring.equals("comboBoxChanged")) {
            JComboBox cb = (JComboBox) ez.getSource();
            String product = (String) cb.getSelectedItem();
            if (product.equals("Book")) {
                c2.show(Center, "Adding book");
                c4.show(West,"Adding book");
                bookorint = 0;

            } else if (product.equals("Electronic")) {
                c2.show(Center, "Adding electronic");
                c4.show(West,"Adding electronic");
                bookorint = 1;

            }
        } else if (command.equals(SReset)) {
            sProductid.setText("");
            sDescription.setText("");
            startyear.setText("");
            endyear.setText("");
        } else if (command.equals(Reset) && bookorint == 0) {
            Productid.setText("");
            Description.setText("");
            Price.setText("");
            Year.setText("");
            Author.setText("");
            Publisher.setText("");
        } else if (command.equals(Reset) && bookorint == 1) {
            eProductid.setText("");
            eDescription.setText("");
            ePrice.setText("");
            eYear.setText("");
            Maker.setText("");
        } else if (command.equals(Add) && bookorint == 0) {
            temp[0] = Productid.getText();
            temp[1] = Description.getText();
            temp[2] = Price.getText();
            temp[3] = Year.getText();
            temp[4] = Author.getText();
            temp[5] = Publisher.getText();
            productsize = h.addbook(temp, addDisplay, products, map, productsize);
        } else if (command.equals(Add) && bookorint == 1) {
            temp[0] = eProductid.getText();
            temp[1] = eDescription.getText();
            temp[2] = ePrice.getText();
            temp[3] = eYear.getText();
            temp[4] = Maker.getText();
            productsize = h.addelectronic(temp, addDisplay, products, map, productsize);
        } else if (command.equals(Search)) {

            temp[0] = sProductid.getText();
            temp[1] = sDescription.getText();
            temp[2] = startyear.getText();
            temp[3] = endyear.getText();
            h.checksearch(products, map, temp, searchDisplay);

        }
        validate();
        repaint();

    }

}
