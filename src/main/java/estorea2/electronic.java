package estorea2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import jdk.jfr.Description;

import java.util.ArrayList;
import java.util.Collections;
public class electronic extends product {
    private String maker;
    public electronic(int ProductID, String Descriptor, String Price, int Year, String Publisher) {
        // for adding to electric object
        this.ProductID = ProductID;
        this.Descriptor = Descriptor;
        this.Price = Price;
        this.Year = Year;
        this.maker = Publisher;
    }
    //for print electronics
    @Override
    public String toString(){
        String padded=String.format("%06d",ProductID);
        
        return "Product ID = '"+ padded + "'\nName = '" + Descriptor + "'\nPrice = '" + Price + "'\nYear = '" +Year+"'\nMaker = '"+ maker +"'";
    }
    @Override public electronic clone(){
        return new electronic(this.ProductID,this.Descriptor,this.Price,this.Year,this.maker);

    } 
}
